import { Router } from 'express'

function createCrudRouter(controller) {
    const router = Router()

    router.post('/:id/update', controller.update)
    router.post('/:id/delete', controller.deleteFn)
    router.put('/:id', controller.update)
    router.delete('/:id', controller.deleteFn)
    router.get('/:id', controller.read)
    router.get('/', controller.listAll)
    router.post('/search', controller.search)
    router.post('/', controller.create)
    return router
}

function createFormsRouter(controller) {
    const router = Router()
    router.get('/:id/new', recipesControllers.showCreateFn)
    router.get('/:id/edit', recipesControllers.showEditFn)
    router.get('/search', recipesControllers.showSearchFn)
    return router
}

export { createCrudRouter,createFormsRouter }