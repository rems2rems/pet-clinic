FROM node:latest
ADD ./package.json /app/
WORKDIR /app
RUN npm install
ADD . .
EXPOSE 8082
VOLUME /app/logs
ENV PORT=8082
CMD node app.js >> /app/logs/app-info.log 2>>/app/logs/app-errors.log 
#ENTRYPOINT [ "node","app.js" ]
