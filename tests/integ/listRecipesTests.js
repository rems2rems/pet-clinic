import { expect } from 'chai'
import request from 'supertest'
import { app } from '../../app.js'
import { createRecipe, listRecipes } from '../../services/recipes.js'

describe("list recipes route", () => {
    it("must add a category", async () => {
        let recipe = {
            _id: "12344556",
            name: "couscous",
            ingredients: ["semoule", "pois chiches", "merguez"],
            needsOven: true,
            needsSpecializedTool: true,
            needsExoticFood: true,
            instructions: ["faites du couscous, c'est bon"]
        }
        createRecipe(recipe)
        let client = request(app)
        let response = await client
            .get("/recipes")
            .set('Accept', 'application/json')
        expect(response.headers["content-type"]).to.match(/json/);
        expect(response.status).to.equal(200);
        expect(response.body[0].name).to.equal('couscous');
        expect(response.body[0].difficulty).to.equal('difficult');
    })
})