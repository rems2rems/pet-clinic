import dotenv from 'dotenv'
dotenv.config()
import express from 'express'
import { createCrudRouter } from './routes/utils.js'
import recipesController from './controllers/recipes.js'

const app = express()

app.get("/hello", (req, res) => {
    res.send("coucou!!!")
})

app.use('/recipes', createCrudRouter(recipesController))

.post(function (req, res, next) {
  // maybe add a new event...
});("/recipes/:id", (req, res) => {
  res.send("coucou!!!")
})

export { app }