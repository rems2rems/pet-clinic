import { categorizeRecipe } from "../categorizeRecipe.js"
import { listRecipes } from "../services/recipes.js"

function create(req, res) {

    res.send("OK")
}

function read(req, res) {
    return res.json({})
}

function listAll(req, res) {
    const recipes = listRecipes()
    // for (const recipe of recipes) {
    //     recipe.difficulty = categorizeRecipe(recipe)
    // }
    return res.json(recipes)
}

function search(req, res) {
    return res.json([])
}

function update(req, res) {
    return res.send("OK")
}

function deleteFn(req, res) {
    return res.send("OK")
}

export default { create, read,listAll,search,update,deleteFn }